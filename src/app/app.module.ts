import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { AlmibarProvider } from './providers/almibar.provider';
import { NotyProvider } from './providers/noty.provider';
import { TokenProvider } from './providers/token.provider';
import { SwalProvider } from './providers/swal.provider';
import { PermissionsProvider } from './providers/permissions.provider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { CombinedPagesModule } from './combined/pages/combined-pages.module';
import { CombinedComponentsModule } from './combined/components/combined-components.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,
    CombinedPagesModule,
    CombinedComponentsModule
  ],
  providers: [
    AlmibarProvider,
    NotyProvider,
    TokenProvider,
    SwalProvider,
    PermissionsProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
