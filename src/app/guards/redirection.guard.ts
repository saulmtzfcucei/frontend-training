import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { TokenProvider } from '../providers/token.provider';
import { variables } from '../providers/variables';

@Injectable({
  providedIn: 'root'
})
export class RedirectionGuard implements CanActivate {

  constructor(private router : Router,
    private tokenProvider : TokenProvider){}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {
      try {
         if(this.tokenProvider.loggedIn){
           this.router.navigate([variables.routes.cobra.home]);
           return false;
         }
         return true;
      } catch (error) {
        console.log(error);
        return false;
      }
  }

}
