import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { NotyProvider } from '../providers/noty.provider';
import { PermissionsProvider } from '../providers/permissions.provider';
import { SwalProvider } from '../providers/swal.provider';

@Injectable({
  providedIn: 'root'
})
export class PermissionsGuard implements CanActivate {

  constructor(private noty : NotyProvider,
    private router : Router,
    private swal : SwalProvider,
    private permissions : PermissionsProvider){}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {

      try {
        this.swal.loading();
        const user = await this.permissions.userPermissions();
        const routes_allowed = this.permissions.getModulePermissions(route.routeConfig?.path);
        this.swal.close();
        if(user){
          for(const role of routes_allowed){
            if(role === user.role){
              return true;
            }
          }
          this.noty.noty_error(`Permisos insuficientes. Contacte al administrador.`, 3000);
          this.router.navigate(['/cobra/home']);
        }else{
          this.noty.noty_error(`La sesión ha caducado.`, 3000);
          this.router.navigate(['/almibar/login']);
        }

        return false;
      } catch (error) {
        console.log(error);
        return false;
      }
  }

}
