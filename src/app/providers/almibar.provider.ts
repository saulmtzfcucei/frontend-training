import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import { first, timeout, catchError } from 'rxjs/operators'
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { NotyProvider } from './noty.provider';
import { HttpDeleteOptions, HttpGetOptions, HttpPostOptions, HttpPutOptions, HttpRequestOptions } from '../models/http.model';
import { environment } from 'src/environments/environment';
import { TokenProvider } from './token.provider';
import { SwalProvider } from './swal.provider';

@Injectable({
  providedIn: 'root'
})
export class AlmibarProvider {

  private _apiURL!: string;
  private _timeout: number = 10000;

  constructor(private http: HttpClient,
    private router : Router,
    private noty : NotyProvider,
    private tokenProvider : TokenProvider,
    private swal : SwalProvider) {
      this._apiURL = environment.api;
    }


  private getRequestOptions(options: HttpRequestOptions){
    const headers = (options.auth) ?
    new HttpHeaders({ 'Accept'  :'application/json', 'authorization' : this.tokenProvider.token}) :
    new HttpHeaders({ 'Accept'  :'application/json' });

    return { headers, body: options.data, params: options.params };
  }

  public post (options: HttpPostOptions) : Observable<any>{
    try {
      if(options.auth && !this.tokenProvider.loggedIn){
        return new Observable(obs => obs.complete());
      }

      const requestOptions : HttpRequestOptions = { auth : options.auth };

      const obs = this.http
      .post(this._apiURL + options.url, options.data, this.getRequestOptions(requestOptions))
      .pipe(timeout(this._timeout))
      .pipe(first())
      .pipe(catchError(this.catchResponseError));

      return obs;

    } catch (error) {
      throw new Error(`Client blew up. ${error}`);
    }
  }

  public get(options: HttpGetOptions) : Observable<any>{
    try {
      if(!(options.auth && this.tokenProvider.token)){
        return new Observable(obs => obs.complete());
      }

      const requestOptions: HttpRequestOptions = { auth  : options.auth };
      const obs = this.http
      .get(this._apiURL + options.url, this.getRequestOptions(requestOptions))
      .pipe(timeout(this._timeout))
      .pipe(first())
      .pipe(catchError(this.catchResponseError));

      return obs;

    } catch (error) {
      throw new Error(`Client blew up. ${error}`);
    }
  }

  public put(options : HttpPutOptions) : Observable<any>{
    try {
      if(!(options.auth && this.tokenProvider.token)){
        return new Observable(obs => obs.complete());
      }

      const requestOptions : HttpRequestOptions = {auth : options.auth };
      const obs = this.http
      .put(this._apiURL + options.url, options.data, this.getRequestOptions(requestOptions))
      .pipe(timeout(this._timeout))
      .pipe(first())
      .pipe(catchError(this.catchResponseError));

      return obs;
    } catch (error) {
      throw new Error(`Client blew up. ${error}`);
    }
  }

  public delete(options: HttpDeleteOptions) : Observable<any>{
    try {
      if(!(options.auth && this.tokenProvider.token)){
        return new Observable(obs => obs.complete());
      }

      const requestOptions : HttpRequestOptions = { auth : options.auth };
      const obs = this.http
      .delete(this._apiURL + options.url, this.getRequestOptions(requestOptions))
      .pipe(timeout(this._timeout))
      .pipe(first())
      .pipe(catchError(this.catchResponseError));

      return obs;
    } catch (error) {
      throw new Error(`Client blew up. ${error}`);
    }
  }

  private catchResponseError = (error: HttpErrorResponse ) => {
    console.log(error);
    try {
      this.swal.close();
      if(error.name){
        if(JSON.parse(JSON.stringify(error.name))==='TimeoutError'){
          this.noty.noty_error('Error de conexión.', 10000);
          this.tokenProvider.destroy_data();
          this.router.navigate(['/almibar/login']);
          return throwError( () => err);
        }
      }
      
      const err = { code: error.status, message: error.error.message || error.message, description: '', development: (error.error as any) };
      if (error.status === 400) {
        err.description = error.error.message;
      } else if (error.status === 401) {
        this.tokenProvider.destroy_data();
        this.noty.noty_error(`No autorizado.`, 3000);
        this.router.navigate(['/almibar/login']);
        return throwError(() => err);
      } else if (error.status === 404) {
        err.description = `El recurso solicitado no existe.`;
      } else if (error.status === 500) {
        err.description = `Error de lado del servidor.`;
      } else if (error.status === 505) {
        err.description = `Error de lado del servidor.`;
      } else if (error.status === 0) {
        err.description = `Error de conexión.`;
      }
      this.noty.noty_error(err.description, 3000);

      return throwError(() => err);

    } catch (error) {
      return throwError(() => error);
    }
  }
}
