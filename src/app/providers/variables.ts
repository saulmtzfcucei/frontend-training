
const combinedBase = '/';
const cobraBase = '/cobra';
const ngCobraBase = '/ngcobra';


export const variables = {
  routes : {
    combined : {
      main : `${combinedBase}/main`
    },

    cobra : {
      _base : `${cobraBase}`,
      login : `${cobraBase}/auth/login`,
      home  : `${cobraBase}/page/home`,
      profile : `${cobraBase}/page/profile`,
      update : `${cobraBase}/page/update`,
      promote : `${cobraBase}/page/promote`,
      delete_my_acc : `${cobraBase}/page/delete_my_acc`,
      deactivate_user : `${cobraBase}/page/deactivate_user`,
      location : `${cobraBase}/page/location`,
      register : `${cobraBase}/page/register`
    },

    cfm : {
      _base : `${cobraBase}`,
      home  : `${cobraBase}/cfm/home`,
      profile : `${cobraBase}/cfm/profile`,
      update : `${cobraBase}/cfm/update`,
      promote : `${cobraBase}/cfm/promote`,
      delete_my_acc : `${cobraBase}/cfm/delete_my_acc`,
      deactivate_user : `${cobraBase}/cfm/deactivate_user`,
      location : `${cobraBase}/cfm/location`,
      register : `${cobraBase}/cfm/register`
    },
    cobrang : {
      _base : `${ngCobraBase}`,
      login : `${ngCobraBase}/auth/login`
    },
    relative : {
      login : '../login',
      home : '../home',
      profile : '../profile',
      update : '../update',
      promote : '../promote',
      delete_my_acc : '../delete_my_acc',
      deactivate_user : `../deactivate_user`,
      location : `../location`,
      register : `../register`
    }
  }
}
 