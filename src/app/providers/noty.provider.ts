import { Injectable } from '@angular/core';
import * as Noty from 'noty';

@Injectable({
  providedIn: 'root'
})
export class NotyProvider {

  constructor() { }

  noty_success(text : string, icon : string, timeout: number){
    new Noty({
      theme: 'relax',
      text: `<i class="fas fa-${icon}"></i> ${text}`,
      layout: 'bottomRight',
      type: 'success',
      animation: {
        open: 'fadeInRightNoty moderateSpeed',
        close: 'fadeOutDownNoty moderateSpeed'
      },
      timeout: timeout,
      progressBar: true,
      closeWith: ['button']
    }).show();
  }

  noty_error(text : string, timeout: number){
    new Noty({
      theme: 'relax',
      text: `<i class="fas fa-times"></i> ${text}`,
      layout: 'bottomRight',
      type: 'error',
      animation: {
        open: 'fadeInRightNoty moderateSpeed',
        close: 'fadeOutDownNoty moderateSpeed'
      },
      timeout: timeout,
      progressBar: true,
      closeWith: ['button']
    }).show();
  }

  noty_info(text : string, timeout : number){
    new Noty({
      theme: 'relax',
      text: `<i class="fas fa-info-circle"></i> ${text}`,
      layout: 'bottomRight',
      type: 'information',
      animation: {
        open: 'fadeInRightNoty moderateSpeed',
        close: 'fadeOutDownNoty moderateSpeed'
      },
      timeout: timeout,
      progressBar: true,
      closeWith: ['button']
    }).show();
  }
}
