import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SwalProvider {
  constructor(private router : Router) { }

  loading(){
    Swal.fire({
      html: `
      <i class="fas fa-circle-notch fa-spin" style="font-size: 40px; color:orange"></i>
      <h2>Cargando...</h5>
      `,
      showConfirmButton: false,
      allowOutsideClick: false,
      backdrop:false,
      width: '350px'
    });
  }

  Swal_small(icon_type : any, text : string, pos:any, time:number, redirect : boolean, path?: string){
    const Toast = Swal.mixin({
      toast: true,
      position: pos,
      showConfirmButton: false,
      timer: time,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })

    Toast.fire({
      icon: icon_type,
      title: text
    }).then(()=>{
      if(redirect){
        this.router.navigate([`${path}`]);
      }
    })
  }

  close(){
    Swal.close();
  }
}
