import { Injectable } from '@angular/core';
import jwt_decode from "jwt-decode";
import { Decoded_JWT } from '../models/jwt.model';

@Injectable({
  providedIn: 'root'
})
export class TokenProvider {

  constructor() { }

  save_token(token : string){
    localStorage.setItem('authorization', token);
    return token;
  }

  destroy_data(){
    localStorage.clear();
  }

  get token (){
    const token = localStorage.getItem('authorization');
    return token ? token : '';
  }

  get uid(){
    const token = localStorage.getItem('authorization');
    if(token){
      const decoded : Decoded_JWT = jwt_decode(token);
      return decoded._id;
    }else{
      return '';
    }
  }

  get loggedIn(){
    return this.token ? true : false;
  }
}
