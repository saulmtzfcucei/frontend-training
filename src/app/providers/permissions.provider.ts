import { Injectable } from '@angular/core';
import { IUserModel } from '../models/user.model';
import { AlmibarProvider } from './almibar.provider';
import { TokenProvider } from './token.provider';

@Injectable({
  providedIn: 'root'
})
export class PermissionsProvider {

  constructor(private almibar : AlmibarProvider,
    private tokenProvider : TokenProvider){}

  public async userPermissions(){
    try {
      const user : IUserModel = await this.almibar.get({
        url: `/user/${this.tokenProvider.uid}`,
        auth: true,
        responseType: 'json'
      }).toPromise();

      return user;

    } catch (error) {
      throw `Error de conexión. ${JSON.stringify(error)}`;
    }
  }

  public getModulePermissions (component : string|undefined){
    const roles_allowed: string[] = [];

    switch (component){
      case 'home' : {
        roles_allowed.push('ADMIN', 'USER');
        break;
      }
      case 'profile' : {
        roles_allowed.push('ADMIN', 'USER');
        break;
      }
      case 'update' : {
        roles_allowed.push('ADMIN', 'USER');
        break;
      }
      case 'promote' : {
        roles_allowed.push('ADMIN');
        break;
      }
      case 'delete_my_acc' : {
        roles_allowed.push('ADMIN');
        break;
      }
      case 'deactivate_user' : {
        roles_allowed.push('ADMIN');
        break;
      }
      case 'location' : {
        roles_allowed.push('ADMIN', 'USER');
        break;
      }
      case 'register' : {
        roles_allowed.push('ADMIN');
        break;
      }
      default : {
        return roles_allowed;
      }
    }
    return roles_allowed;
  }

}
