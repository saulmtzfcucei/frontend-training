import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CombinedLayoutComponent } from './combined/pages/combined-layout/combined-layout.component';
import { MainSelectorComponent } from './combined/pages/main-selector/main-selector.component';


const routes: Routes = [
  {
    path: 'cobra',
    loadChildren: () => import('./cobra/cobra.module').then(m => m.CobraModule)
  },
  {
    path: 'ngcobra',
    loadChildren: () => import('./cobrang/cobrang.module').then(m => m.CobrangModule)
  },
  {
    path: 'landing',
    component: CombinedLayoutComponent,
    children : [
      {
        path: 'main',
        component: MainSelectorComponent
      },
      {
        path: '**',
        pathMatch: 'full',
        redirectTo: 'main'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'landing',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }


// {
//   path: 'almibar',
//   loadChildren : () => import('./auth/auth.module').then( m => m.AuthModule )
// },
// {
//   path: 'cobra',
//   loadChildren : () => import('./pages/pages.module').then(m => m.PagesModule )
// },
// {
//   path: '404',
//   loadChildren: () => import('./shared/error.module').then(m => m.ErrorModule )
// },
// {
//   path: 'cfm',
//   loadChildren: () => import('./cfm/cfm.module').then( m => m.CfmModule )
// },
// {
//   path: 'combined',
//   loadChildren : () => import('./combined/combined.module').then(m => m.CombinedModule )
// },
// {
//   path: 'cobrang',
//   loadChildren : () => import('./cobrang/cobrang.module').then( m => m.CobrangModule )
// },
// {
//   path: '',
//   pathMatch: 'full',
//   redirectTo: 'combined/main'
// },
// {
//   path: '**',
//   pathMatch: 'full',
//   redirectTo : '404'
// }
