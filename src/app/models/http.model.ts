export interface HttpDefaultOptions {
  ignoreBase?: boolean;
  url: string;
  auth: boolean;
}

export interface HttpRequestOptions {
  auth?: boolean;
  data?: any;
  params?: any;
  responseType?: any;
}

export interface HttpPostOptions extends HttpDefaultOptions {
  params?: any;
  responseType?: any;
  data? :any;
}

export interface HttpGetOptions extends HttpDefaultOptions {
  params? : any;
  responseType? : any;
}

export interface HttpPutOptions extends HttpDefaultOptions {
  params? : any;
  data? : any;
  responseType? : any;
}

export interface HttpDeleteOptions extends HttpDefaultOptions {
  params? : any;
  responseType? : any;
}
