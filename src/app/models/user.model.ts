export interface IUserModel {
  _id : string,
  username: string,
  email: string,
  password : string,
  role?: string,
  deleted? : boolean
}
