export interface Decoded_JWT {
  exp : number,
  iat : number,
  _id : string
}

export interface JWT {
  token ? : string
}
