import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesLayoutComponent } from './pages/layout/pages-layout/pages-layout.component';
import { HomeComponent } from './pages/home/home.component';
import { PermissionsGuard } from '../guards/permissions.guard';
import { ProfileComponent } from './pages/profile/profile.component';
import { UpdateComponent } from './pages/update/update.component';
import { PromoteComponent } from './pages/promote/promote.component';
import { DeleteMyAccComponent } from './pages/delete-my-acc/delete-my-acc.component';
import { DeactivateUserComponent } from './pages/deactivate-user/deactivate-user.component';
import { LocationComponent } from './pages/location/location.component';
import { RegisterComponent } from './pages/register/register.component';
import { RedirectionGuard } from '../guards/redirection.guard';
import { LoginComponent } from './pages/login/login.component';
import { AuthLayoutComponent } from './pages/layout/auth-layout/auth-layout.component';
import { CfmLayoutComponent } from './pages/layout/cfm-layout/cfm-layout.component';

const routes : Routes = [
  {
    path: 'page',
    component: PagesLayoutComponent,
    children : [
      {
        path: 'home',
        component: HomeComponent,
        canActivate: [PermissionsGuard]
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [PermissionsGuard]
      },
      {
        path: 'update',
        component: UpdateComponent,
        canActivate:[PermissionsGuard],
      },
      {
        path: 'promote',
        component: PromoteComponent,
        canActivate:[PermissionsGuard],
      },

      {
        path: 'delete_my_acc',
        component: DeleteMyAccComponent,
        canActivate:[PermissionsGuard]
      },
      {
        path: 'deactivate_user',
        component: DeactivateUserComponent,
        canActivate:[PermissionsGuard]
      },
      {
        path: 'location',
        component: LocationComponent,
        canActivate:[PermissionsGuard]
      },
      {
        path: 'register',
        component: RegisterComponent,
        canActivate:[PermissionsGuard]
      },
      {
        path: '**',
        pathMatch: 'full',
        redirectTo: 'home'
      },
    ]
  },
  {
    path: 'cfm',
    component: CfmLayoutComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
        canActivate: [PermissionsGuard]
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [PermissionsGuard]
      },
      {
        path: 'update',
        component: UpdateComponent,
        canActivate:[PermissionsGuard],
      },
      {
        path: 'promote',
        component: PromoteComponent,
        canActivate:[PermissionsGuard],
      },

      {
        path: 'delete_my_acc',
        component: DeleteMyAccComponent,
        canActivate:[PermissionsGuard]
      },
      {
        path: 'deactivate_user',
        component: DeactivateUserComponent,
        canActivate:[PermissionsGuard]
      },
      {
        path: 'location',
        component: LocationComponent,
        canActivate:[PermissionsGuard]
      },
      {
        path: 'register',
        component: RegisterComponent,
        canActivate:[PermissionsGuard]
      },
      {
        path: '**',
        pathMatch: 'full',
        redirectTo: 'home'
      },
    ]
  },
  {
    path: 'auth',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
        canActivate:[RedirectionGuard]
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CobraRoutingModule { }
