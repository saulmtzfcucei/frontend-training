import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IUserModel } from 'src/app/models/user.model';
import { AlmibarProvider } from 'src/app/providers/almibar.provider';
import { NotyProvider } from 'src/app/providers/noty.provider';
import { TokenProvider } from 'src/app/providers/token.provider';
import { variables } from 'src/app/providers/variables';


@Component({
  selector: 'app-side-cfm-menu',
  templateUrl: './side-cfm-menu.component.html',
  styleUrls: ['./side-cfm-menu.component.scss']
})
export class SideMenuCFMComponent {

  main_cfm_img = 'https://cdn-icons-png.flaticon.com/512/3767/3767084.png';

  user_modules: String[] = ['title', 'home', 'cambiar_usuario', 'perfil', 'actualizar_info', 'location'];
  admin_modules: String[] = ['title', 'registro_usuario', 'promover_usuario','delete_user', 'deactivate_user', 'location'];

  allowed_modules: String[] = [];

  role! : string|undefined;

  // Routes
  mainR : string = variables.routes.combined.main;
  cfmHomeR : string = variables.routes.cfm.home;
  cfmLocationR : string = variables.routes.cfm.location;
  cfmProfileR : string = variables.routes.cfm.profile;
  cfmRegisterR : string = variables.routes.cfm.register;
  cfmUpdateR : string = variables.routes.cfm.update;
  cfmPromoteR : string = variables.routes.cfm.promote;
  cfmDeleteMyAccR : string = variables.routes.cfm.delete_my_acc;
  cfmDeactivateUserR : string = variables.routes.cfm.deactivate_user;


  constructor(private almibar : AlmibarProvider,
    private tokenProvider :TokenProvider,
    private router : Router,
    private noty : NotyProvider) {
      this.check_permits();
  }


  async check_permits(){
    try {
      const user : IUserModel = await this.almibar.get({
        url: `/user/${this.tokenProvider.uid}`,
        auth: true,
        responseType: 'json'
      }).toPromise();


      if(user){
        this.role = user.role;
        if(user.role === 'USER'){
          this.allowed_modules = this.user_modules;
        }
        if(user.role === 'ADMIN'){
          this.allowed_modules = [...this.user_modules, ...this.admin_modules];
        }
      }else{
        this.noty.noty_error(`La sesión ha caducado.`, 3000);
        this.router.navigate([this.mainR]);
      }
    } catch (error) {
      console.log(error);
    }
  }

  is_module_allowed( module : string ){
    return this.allowed_modules.includes(module);
  }

  logout(){
    if(window.confirm('Para cambiar de usuario es necesario cerrar sesión. ¿Deseas continuar?')){
      this.tokenProvider.destroy_data();
      this.router.navigate([this.mainR]);
      this.noty.noty_info('Tu sesión ha finalizado.', 3000);
    }
  }

}
