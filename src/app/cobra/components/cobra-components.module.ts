import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CobraRoutingModule } from '../cobra-routing.module';
import { SideMenuCFMComponent } from './cfm-components/side-cfm-menu/side-cfm-menu.component';
import { FooterComponent } from './cobra-components/footer/footer.component';
import { HeaderComponent } from './cobra-components/header/header.component';
import { NavbarComponent } from './cobra-components/navbar/navbar.component';
import { SideMenuComponent } from './cobra-components/side-menu/side-menu.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    SideMenuCFMComponent,
    FooterComponent,
    HeaderComponent,
    NavbarComponent,
    SideMenuComponent
  ],
  imports: [
    CommonModule,
    CobraRoutingModule,
    RouterModule
  ],
  exports : [
    SideMenuCFMComponent,
    FooterComponent,
    HeaderComponent,
    NavbarComponent,
    SideMenuComponent
  ]
})
export class CobraComponentsModule { }
