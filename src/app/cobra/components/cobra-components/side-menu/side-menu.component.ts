import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IUserModel } from 'src/app/models/user.model';
import { AlmibarProvider } from 'src/app/providers/almibar.provider';
import { NotyProvider } from 'src/app/providers/noty.provider';
import { TokenProvider } from 'src/app/providers/token.provider';
import { variables } from 'src/app/providers/variables';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent{


  user_modules: String[] = ['title', 'home', 'cambiar_usuario', 'perfil', 'actualizar_info', 'location'];
  admin_modules: String[] = ['title', 'registro_usuario', 'promover_usuario','delete_user', 'deactivate_user', 'location'];

  allowed_modules: String[] = [];

  role! : string|undefined;

  // HTML image paths
  home_img = 'https://icons-for-free.com/iconfiles/png/512/home-131964752879605498.png';
  cambiar_usuario_img = 'https://cdn-icons-png.flaticon.com/512/3101/3101003.png';
  registrarme_img = 'https://www.pinclipart.com/picdir/big/98-988072_go-to-image-add-user-icon-clipart.png';
  mi_perfil_img = 'https://cdn1.iconfinder.com/data/icons/avatars-1-5/136/87-512.png';
  actualizar_datos_img = 'https://cpgcanhelp.com/wp-content/uploads/2018/03/Technical-Writing.png';
  promover_usuario_img = 'https://cdn-icons-png.flaticon.com/512/1469/1469840.png';
  delete_img = 'https://cdn4.iconfinder.com/data/icons/social-messaging-ui-coloricon-1/21/52-512.png';
  deactivate_img = 'https://cdn0.iconfinder.com/data/icons/user-administrator-web-ui-app/100/user-07-512.png';
  location_img = 'https://cdn.iconscout.com/icon/free/png-256/location-3079544-2561454.png';
  // -----------------

  //Routes
  mainR : string = variables.routes.combined.main;
  homeR : string = variables.routes.cobra.home;
  registerR : string = variables.routes.cobra.register;
  profileR : string = variables.routes.cobra.profile;
  updateR : string = variables.routes.cobra.update;
  promoteR : string = variables.routes.cobra.promote;
  delete_my_accR : string = variables.routes.cobra.delete_my_acc;
  deactivate_userR : string = variables.routes.cobra.deactivate_user;
  locationR : string = variables.routes.cobra.location;


  constructor(private almibar : AlmibarProvider,
    private tokenProvider :TokenProvider,
    private router : Router,
    private noty : NotyProvider) {
      this.check_permits();
  }

  async check_permits(){
    try {
      const user : IUserModel = await this.almibar.get({
        url: `/user/${this.tokenProvider.uid}`,
        auth: true,
        responseType: 'json'
      }).toPromise();


      if(user){
        this.role = user.role;
        if(user.role === 'USER'){
          this.allowed_modules = this.user_modules;
        }
        if(user.role === 'ADMIN'){
          this.allowed_modules = [...this.user_modules, ...this.admin_modules];
        }
      }else{
        this.noty.noty_error(`La sesión ha caducado.`, 3000);
        this.router.navigate([this.mainR]);
      }
    } catch (error) {
      console.log(error);
    }
  }

  is_module_allowed( module : string ){
    return this.allowed_modules.includes(module);
  }

  logout(){
    if(window.confirm('Para cambiar de usuario es necesario cerrar sesión. ¿Deseas continuar?')){
      this.tokenProvider.destroy_data();
      this.router.navigate([this.mainR]);
      this.noty.noty_info('Tu sesión ha finalizado.', 3000);
    }
  }



}
