import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NotyProvider } from 'src/app/providers/noty.provider';
import { TokenProvider } from 'src/app/providers/token.provider';
import { variables } from 'src/app/providers/variables';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent{

    // HTML image paths
    cobra_icon_img = 'https://www.pinclipart.com/picdir/big/121-1210042_viper-symbol-viper-snake-logo-png-clipart.png';
    cobra_letters_img = '../../../assets/img/cobra.png'
    home_img = 'https://icons-for-free.com/iconfiles/png/512/home-131964752879605498.png'
    logout_img = 'https://www.liceomodernosanmarcos.edu.co/wp-content/uploads/2020/09/salida.svg';
    folder_img = 'https://cdn-icons-png.flaticon.com/512/3767/3767084.png';
    // ----------------

    //Routes
    mainR : string = variables.routes.combined.main;
    homeR : string = variables.routes.cobra.home;
    cfmHomeR : string = variables.routes.cfm.home;


  constructor(private tokenProvider : TokenProvider,
    private router : Router,
    private noty : NotyProvider) {
    }

  get logged_in(){
    return this.tokenProvider.loggedIn;
  }

  close_sesion(){
    this.tokenProvider.destroy_data();
    this.noty.noty_info('Has cerrado sesión', 3000);
    this.router.navigate([this.mainR]);
  }
}
