import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CobraPagesModule } from './pages/cobra-pages.module';
import { CobraRoutingModule } from './cobra-routing.module';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CobraPagesModule,
    CobraRoutingModule,
    RouterModule,
    MaterialModule
  ]
})
export class CobraModule { }
