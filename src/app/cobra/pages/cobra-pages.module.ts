import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CobraComponentsModule } from '../components/cobra-components.module';
import { DeactivateUserComponent } from './deactivate-user/deactivate-user.component';
import { DeleteMyAccComponent } from './delete-my-acc/delete-my-acc.component';
import { ErrorCfmComponent } from './error-cfm/error-cfm.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { HomeComponent } from './home/home.component';
import { AuthLayoutComponent } from './layout/auth-layout/auth-layout.component';
import { CfmLayoutComponent } from './layout/cfm-layout/cfm-layout.component';
import { ErrorLayoutComponent } from './layout/error-layout/error-layout.component';
import { PagesLayoutComponent } from './layout/pages-layout/pages-layout.component';
import { LocationComponent } from './location/location.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { PromoteComponent } from './promote/promote.component';
import { RegisterComponent } from './register/register.component';
import { UpdateComponent } from './update/update.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';



@NgModule({
  declarations: [
    DeactivateUserComponent,
    DeleteMyAccComponent,
    ErrorCfmComponent,
    ErrorPageComponent,
    HomeComponent,
    AuthLayoutComponent,
    CfmLayoutComponent,
    ErrorLayoutComponent,
    PagesLayoutComponent,
    LocationComponent,
    LoginComponent,
    ProfileComponent,
    PromoteComponent,
    RegisterComponent,
    UpdateComponent
  ],
  imports: [
    CommonModule,
    CobraComponentsModule,
    ReactiveFormsModule,
    RouterModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAfgPd6043llJOKCJeM3q2kbQJ5fvmYoD4',
      libraries: ['places']
    })

  ]
})
export class CobraPagesModule { }
