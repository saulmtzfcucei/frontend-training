import { Component } from '@angular/core';
import { IUserModel } from 'src/app/models/user.model';
import { AlmibarProvider } from 'src/app/providers/almibar.provider';
import { TokenProvider } from 'src/app/providers/token.provider';
import { variables } from 'src/app/providers/variables';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {

  page_title = 'Perfil';

  woman_img='https://cdn-icons-png.flaticon.com/512/146/146025.png';
  man_img='https://cdn.iconscout.com/icon/free/png-512/avatar-370-456322.png';

  user! : IUserModel;

  // Routes
  relativePathUpdate : string = variables.routes.relative.update;

  constructor(private almibar : AlmibarProvider,
    private tokenProvider : TokenProvider) {
      this.getUser();
    }

  async getUser(){
      const user : IUserModel = await this.almibar.get({
        url: `/user/${this.tokenProvider.uid}`,
        auth: true,
        responseType: 'json'
      }).toPromise();

      if(user){
        this.user = user;
      }
  }
}
