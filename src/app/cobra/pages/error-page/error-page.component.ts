import { Component } from '@angular/core';
import { variables } from 'src/app/providers/variables';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent {

  page_title = 'Página no encontrada';

  relativePathHome : string = variables.routes.relative.home;

  constructor() { }


}
