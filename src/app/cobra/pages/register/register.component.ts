import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IUserModel } from 'src/app/models/user.model';

import { AlmibarProvider } from 'src/app/providers/almibar.provider';
import { NotyProvider } from 'src/app/providers/noty.provider';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  page_title = 'Registrar nuevo usuario';
  pick_img='../../../assets/img/pick.png';

  form!: FormGroup;

  constructor(private fb: FormBuilder,
    private almibar : AlmibarProvider,
    private noty : NotyProvider) {
    this.createForm();
  }

  get emailNotValid(){
    return this.form.get('email')?.invalid && this.form.get('email')?.touched
  }

  get usernameNotValid(){
    return this.form.get('username')?.invalid && this.form.get('username')?.touched
  }

 get passwordNotValid(){
    return this.form.get('password')?.invalid && this.form.get('password')?.touched
 }

  createForm(){
    this.form = this.fb.group({
      email   : ['', [Validators.required, Validators.pattern(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)]],
      username: ['', [Validators.required, Validators.pattern(/^[0-9a-zA-Z]{4,15}$/)]],
      password: ['', [Validators.required, Validators.pattern(/^\w(?=.*?[#?!@$%^&*-]).{6,20}$/)]]
    });
  }

  public async save(){

    if(this.form.invalid){
      if(this.form.controls['email'].status === 'INVALID'){
        this.noty.noty_error('El email debe contener el nombre de usuario, el signo @ (arroba) y el dominio.', 7000);
        this.form.controls['email'].reset();
      }
      if(this.form.controls['username'].status === 'INVALID'){
        this.noty.noty_error('El nombre puede tener de 4 a 15 caracteres, números y letras. No se permiten caracteres especiales', 7000);
        this.form.controls['username'].reset();
      }
      if(this.form.controls['password'].status === 'INVALID'){
        this.noty.noty_error('La contraseña puede tener de 6 a 20 caracteres, números y letras. Debe tener al menos uno de los siguientes signos: #?!@$%^&*-', 7000);
        this.form.controls['password'].reset();
      }
      this.form.reset();
      Object.values(this.form.controls).forEach( control => {
        control.markAsTouched();
      });
    }

    if(this.form.valid){

      try {
        const user : IUserModel = await this.almibar.post({
          url: '/user',
          auth: false,
          responseType: 'json',
          data: this.form.value
        }).toPromise();

        if( user ){
          this.form.reset();
          this.noty.noty_success(`El usuario ${user.username} ha sido creado`, 'user', 3000);
        }
      } catch (error) {
        console.log(error);
      }
    }
  }

}
