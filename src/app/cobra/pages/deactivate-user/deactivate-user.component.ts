import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IUserModel } from 'src/app/models/user.model';
import { AlmibarProvider } from 'src/app/providers/almibar.provider';
import { NotyProvider } from 'src/app/providers/noty.provider';
import { TokenProvider } from 'src/app/providers/token.provider';
import { variables } from 'src/app/providers/variables';

@Component({
  selector: 'app-deactivate-user',
  templateUrl: './deactivate-user.component.html',
  styleUrls: ['./deactivate-user.component.scss']
})
export class DeactivateUserComponent{

  page_title = 'Desactivar usuario'

  snake_img = 'https://www.pinclipart.com/picdir/big/121-1210042_viper-symbol-viper-snake-logo-png-clipart.png';

  //Routes
  relativePathHome : string = variables.routes.relative.home;
  mainR : string = variables.routes.combined.main;

  form! : FormGroup;

  constructor(private noty : NotyProvider,
    private almibar : AlmibarProvider,
    private fb : FormBuilder,
    private router : Router,
    private tokenProvider : TokenProvider) {
      this.createForm();
    }

  get idNotValid(){
    return this.form.get('id')?.invalid && this.form.get('id')?.touched;
  }

  public async deactivate(){
    try {
      if(window.confirm('Esta acción es irreversible. ¿Deseas continuar?')){
        if(this.form.invalid){
          if(this.form.controls['id'].status === 'INVALID'){
            this.noty.noty_error('El id de usuario tiene 24 digitos, números del 0-9 y letras de la a-f.', 7000);
          }

          Object.values(this.form.controls).forEach( control => {
            control.reset();
            control.markAsTouched();
          });
        }

        if(this.form.valid){
          try {
            const user : IUserModel = await this.almibar.delete({
              url: `/user/${this.form.controls['id'].value}`,
              auth: true,
              responseType: 'json'
            }).toPromise();

            if(user){
              this.noty.noty_success(`El usuario ${user.username} fue eliminado`, 'trash', 3000);
              this.form.reset();
            }else{
              this.tokenProvider.destroy_data();
              this.noty.noty_error(`La sesión ha caducado.`, 3000);
              this.router.navigate([this.mainR]);
            }
          } catch (error) {
            console.log(error);
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  createForm(){
    this.form = this.fb.group({
      id : ['', [Validators.required, Validators.pattern(/^[0-9a-fA-F]{24}$/)]]
    });
  }

}
