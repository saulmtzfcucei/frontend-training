import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IUserModel } from 'src/app/models/user.model';
import { AlmibarProvider } from 'src/app/providers/almibar.provider';
import { NotyProvider } from 'src/app/providers/noty.provider';
import { TokenProvider } from 'src/app/providers/token.provider';
import { variables } from 'src/app/providers/variables';

@Component({
  selector: 'app-delete-my-acc',
  templateUrl: './delete-my-acc.component.html',
  styleUrls: ['./delete-my-acc.component.scss']
})
export class DeleteMyAccComponent {

  page_title = 'Eliminar mi cuenta';

  snake_img = 'https://www.pinclipart.com/picdir/big/121-1210042_viper-symbol-viper-snake-logo-png-clipart.png';

  // Routes
  mainR : string = variables.routes.combined.main;
  relativePathHome : string = variables.routes.relative.home;

  constructor(private almibar : AlmibarProvider,
    private noty : NotyProvider,
    private tokenProvider : TokenProvider,
    private router : Router) {}

  async delete(){
    try {
      if(window.confirm('Estoy consciente de que esta acción es irreversible')){

        const deleted_user : IUserModel = await this.almibar.delete({
          url: `/user/${this.tokenProvider.uid}`,
          auth: true,
          responseType: 'json'
        }).toPromise();

        this.tokenProvider.destroy_data();

        if(deleted_user){
          this.router.navigate([this.mainR]);
          this.noty.noty_info(`La cuenta del usuario ${deleted_user.username} ha sido eliminada.`, 3000);
        }else{
          this.noty.noty_error(`La sesión ha caducado.`, 3000);
          this.router.navigate([this.mainR]);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

}
