import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IUserModel } from 'src/app/models/user.model';
import { AlmibarProvider } from 'src/app/providers/almibar.provider';
import { NotyProvider } from 'src/app/providers/noty.provider';

@Component({
  selector: 'app-promote',
  templateUrl: './promote.component.html',
  styleUrls: ['./promote.component.scss']
})
export class PromoteComponent{

  page_title = 'Promover usuario a administrador'

  form! : FormGroup;


  constructor(private fb : FormBuilder,
    private almibar : AlmibarProvider,
    private noty : NotyProvider) {
      this.createForm();
    }

  get idNotValid(){
    return this.form.get('id')?.invalid && this.form.get('id')?.touched
  }

  public async promote(){

    if(this.form.invalid){
      if(this.form.controls['id'].status === 'INVALID'){
        this.noty.noty_error('El id de usuario tiene 24 digitos, números del 0-9 y letras de la a-f.', 7000);
      }

      Object.values(this.form.controls).forEach( control => {
        control.reset();
        control.markAsTouched();
      });
    }

    if(this.form.valid){
      try {
        const user : IUserModel = await this.almibar.put({
          url: `/user/${this.form.controls['id'].value}`,
          auth: true,
          responseType: 'json',
          data: {role : 'ADMIN'}
        }).toPromise();

        if(user){
          this.noty.noty_success(`El usuario ${user.username} es ahora ${user.role}`, 'user', 3000);
          this.form.reset();
        }

      } catch (error) {
        console.log(error);
      }
    }
  }

  createForm(){
    this.form = this.fb.group({
      id : ['', [Validators.required, Validators.pattern(/^[0-9a-fA-F]{24}$/)]]
    });
  }

}
