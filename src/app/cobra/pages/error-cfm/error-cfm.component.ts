import { Component } from '@angular/core';
import { SwalProvider } from 'src/app/providers/swal.provider';
import { variables } from 'src/app/providers/variables';

@Component({
  selector: 'app-error-cfm',
  templateUrl: './error-cfm.component.html',
  styleUrls: ['./error-cfm.component.scss']
})
export class ErrorCfmComponent {

  constructor(private swal : SwalProvider) {
    this.swal.Swal_small('error', 'No tienes acceso a esta ubicación', 'center', 5000, true, variables.routes.cfm.home);

  }
}
