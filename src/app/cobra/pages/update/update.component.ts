import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IUserModel } from 'src/app/models/user.model';
import { AlmibarProvider } from 'src/app/providers/almibar.provider';
import { NotyProvider } from 'src/app/providers/noty.provider';
import { TokenProvider } from 'src/app/providers/token.provider';
import { variables } from 'src/app/providers/variables';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent {

  page_title = 'Actualizar perfil';

  woman_img='https://cdn-icons-png.flaticon.com/512/146/146025.png';
  man_img='https://cdn.iconscout.com/icon/free/png-512/avatar-370-456322.png';

  user! : IUserModel;
  _id! : string;

  form!: FormGroup;

  // Routes
  mainR : string = variables.routes.combined.main;
  relativePathProfile : string = variables.routes.relative.profile;

  constructor(private fb: FormBuilder,
    private almibar : AlmibarProvider,
    private tokenProvider : TokenProvider,
    private noty : NotyProvider,
    private router : Router) {
      this.getUser();
      this.createForm();
    }

  get emailNotValid(){
    return (this.form.get('email')?.touched && !this.form.get('email')?.value) ||
    (this.form.get('email')?.value && this.form.get('email')?.invalid);
  }

  get usernameNotValid(){
    return (this.form.get('username')?.touched && !this.form.get('username')?.value) ||
    (this.form.get('username')?.value && this.form.get('username')?.invalid);
  }

 get passwordNotValid(){
  return (this.form.get('password')?.touched && !this.form.get('password')?.value) ||
  (this.form.get('password')?.value && this.form.get('password')?.invalid);
 }

  createForm(){
    this.form = this.fb.group({
      username: ['', [Validators.pattern(/^[0-9a-zA-Z]{4,15}$/)]],
      email   : ['', [Validators.pattern(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)]],
      password: ['', [Validators.pattern(/^\w(?=.*?[#?!@$%^&*-]).{6,20}$/)]]
    });
  }

  public async update(){
    if(!this.form.pristine && this.form.valid){
      let put_payload = this.form.value;

      Object.entries(put_payload).forEach( field => {
        if(!field[1]) delete put_payload[`${field[0]}`];
      });

      if(Object.keys(put_payload).length > 0){
        this.put_request(put_payload);
      }else{
        this.noty.noty_error('Formulario vacío', 3000);
      }
    }else{
      this.invalid_form_info();
    }
  }

  private async put_request(put_payload : Object){
    try {
      const user : IUserModel = await this.almibar.put({
        url: `/user/${this.tokenProvider.uid}`,
        auth: true,
        responseType: 'json',
        data: put_payload
      }).toPromise();

      if(user){
          this.user = user;
          this.noty.noty_success(`El usuario ha sido actualizado`, 'user', 3000);
          this.form.reset();
      }else{
        this.noty.noty_error(`La sesión ha caducado.`, 3000);
        this.router.navigate([this.mainR]);
      }
    } catch (error) {
      console.log(error);
    }
  }

  public invalid_form_info(){
    if(this.form.pristine){
      this.noty.noty_error('No se han indicado cambios', 3000);
    }
    if(this.form.controls['email'].status === 'INVALID' &&  this.form.controls['email'].value){
        this.noty.noty_error('El email debe contener el nombre de usuario, el signo @ (arroba) y el dominio.', 7000);
        this.form.controls['email'].reset();
        this.form.controls['email'].markAsTouched();
    }
    if(this.form.controls['username'].status==='INVALID' &&  this.form.controls['username'].value){
      this.noty.noty_error('El nombre puede tener de 4 a 15 caracteres, números y letras. No se permiten caracteres especiales', 7000);
      this.form.controls['username'].reset();
      this.form.controls['username'].markAsTouched();
    }
    if(this.form.controls['password'].status==='INVALID' &&  this.form.controls['password'].value){
      this.noty.noty_error('La contraseña puede tener de 6 a 20 caracteres, números y letras. Debe tener al menos uno de los siguientes signos: #?!@$%^&*-', 7000);
      this.form.controls['password'].reset();
      this.form.controls['password'].markAsTouched();
    }
  }

  public async getUser(){
    const user : IUserModel = await this.almibar.get({
      url: `/user/${this.tokenProvider.uid}`,
      auth: true,
      responseType: 'json'
    }).toPromise();

    if(user){
      this.user = user;
    }
  }
}
