import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JWT } from 'src/app/models/jwt.model';

import { AlmibarProvider } from 'src/app/providers/almibar.provider';
import { NotyProvider } from 'src/app/providers/noty.provider';
import { TokenProvider } from 'src/app/providers/token.provider';
import { variables } from 'src/app/providers/variables';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  page_title = 'Iniciar sesión';
  pick_img='../../../assets/img/pick.png';

  // routes
  homeR : string = variables.routes.cobra.home;
  mainR : string = variables.routes.combined.main;

  form!: FormGroup;

  constructor(private fb: FormBuilder,
    private almibar : AlmibarProvider,
    private noty : NotyProvider,
    private router : Router,
    private tokenProvider : TokenProvider) {
      this.createForm();
  }

  get emailNotValid(){
    return this.form.get('email')?.invalid && this.form.get('email')?.touched
  }

 get passwordNotValid(){
    return this.form.get('password')?.invalid && this.form.get('password')?.touched
 }

  createForm(){
    this.form = this.fb.group({
      email   : ['', [Validators.required, Validators.pattern(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)]],
      password: ['', [Validators.required, Validators.pattern(/^\w(?=.*?[#?!@$%^&*-]).{6,20}$/)]]
    });
  }

  public async login(){
    if(this.form.invalid){
      if(this.form.controls['email'].status === 'INVALID'){
        this.noty.noty_error('El email debe contener el nombre de usuario, el signo @ (arroba) y el dominio.' , 7000);
      }
      if(this.form.controls['password'].status === 'INVALID'){
        this.noty.noty_error('La contraseña puede tener de 6 a 20 caracteres, números y letras. Debe tener al menos uno de los siguientes signos: #?!@$%^&*-' , 7000);
      }
      this.form.reset();
      Object.values(this.form.controls).forEach( control => {
        control.markAsTouched();
      });

    }

    if(this.form.valid){
      try {
        const token : JWT = await this.almibar.post({
          url: '/login',
          auth: false,
          responseType: 'json',
          data: this.form.value
          }).toPromise();
          console.log(token);

          if( token.token ){
            this.tokenProvider.save_token(token.token);
            this.router.navigate([this.homeR]);
            this.noty.noty_success(`Has iniciado sesión`, 'check' , 3000);
          }else{
            this.noty.noty_error(`La sesión ha caducado.`, 3000);
            this.router.navigate([this.mainR]);
          }
      } catch (error) {
        console.log(error);
      }
    }
  }
}
