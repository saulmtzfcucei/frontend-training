import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatRippleModule } from '@angular/material/core';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';

const AngularMaterialModules = [
  MatToolbarModule,
  MatRippleModule,
  MatMenuModule,
  MatIconModule
]

@NgModule({
  imports : AngularMaterialModules,
  exports : AngularMaterialModules
})
export class MaterialModule { }
