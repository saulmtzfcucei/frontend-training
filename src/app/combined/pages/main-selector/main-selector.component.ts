import { Component, OnInit } from '@angular/core';
import { variables } from 'src/app/providers/variables';

@Component({
  selector: 'app-main-selector',
  templateUrl: './main-selector.component.html',
  styleUrls: ['./main-selector.component.scss']
})
export class MainSelectorComponent implements OnInit {

  // HTML Routes
  html_img = 'https://logos-download.com/wp-content/uploads/2017/07/HTML5_badge.png';
  angularmat_img = 'https://d2eip9sf3oo6c2.cloudfront.net/tags/images/000/000/300/full/angular2.png'

  // Routes
  loginR = variables.routes.cobra.login;
  ngloginR = variables.routes.cobrang.login;

  //Ripple
  centered = false;
  disabled = false;
  unbounded = true;

  radius: number = 300;
  color: string = 'green';
  constructor() { }

  ngOnInit(): void {
  }

}
