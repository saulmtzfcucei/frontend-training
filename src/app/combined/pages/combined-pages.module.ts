import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CombinedComponentsModule } from '../components/combined-components.module';
import { CombinedLayoutComponent } from './combined-layout/combined-layout.component';
import { MainSelectorComponent } from './main-selector/main-selector.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material/material.module';
// import { CobrangComponentsModule } from 'src/app/cobrang/components/cobrang-components.module';
// import { CobraComponentsModule } from 'src/app/cobra/components/cobra-components.module';



@NgModule({
  declarations: [
    CombinedLayoutComponent,
    MainSelectorComponent
  ],
  imports: [
    CommonModule,
    CombinedComponentsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule
  ]
})
export class CombinedPagesModule { }
