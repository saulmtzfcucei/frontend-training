import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CombinedRoutingModule } from './combined-routing.module';
import { CombinedPagesModule } from './pages/combined-pages.module';
import { CombinedComponentsModule } from './components/combined-components.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CombinedRoutingModule,
    CombinedPagesModule,
    CombinedComponentsModule,
    RouterModule
  ]
})
export class CombinedModule { }
