import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CombinedLayoutComponent } from './pages/combined-layout/combined-layout.component';
import { MainSelectorComponent } from './pages/main-selector/main-selector.component';

const routes : Routes = [
  {
    path: 'cobra',
    loadChildren: () => import('../cobra/cobra.module').then(m => m.CobraModule)
  },
  {
    path: 'ngcobra',
    loadChildren: () => import('../cobrang/cobrang.module').then(m => m.CobrangModule)
  },
  {
    path: '',
    component: CombinedLayoutComponent,
    children : [
      {
        path: 'main',
        component: MainSelectorComponent
      },
      {
        path: '**',
        pathMatch: 'full',
        redirectTo: 'main'
      }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild( routes )
  ],
  exports : [
    RouterModule
  ]
})
export class CombinedRoutingModule { }
