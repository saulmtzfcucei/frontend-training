import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CombinedRoutingModule } from '../combined-routing.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CombinedRoutingModule,
    RouterModule
  ]
})
export class CombinedComponentsModule { }
