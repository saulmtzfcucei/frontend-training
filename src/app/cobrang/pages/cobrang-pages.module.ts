import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CobrangComponentsModule } from '../components/cobrang-components.module';
import { CobrangLayoutComponent } from './cobrang-layout/cobrang-layout.component';
import { NgloginComponent } from './nglogin/nglogin.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';



@NgModule({
  declarations: [
    CobrangLayoutComponent,
    NgloginComponent
  ],
  imports: [
    CommonModule,
    CobrangComponentsModule,
    ReactiveFormsModule,
    RouterModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAfgPd6043llJOKCJeM3q2kbQJ5fvmYoD4',
      libraries: ['places']
    })

  ]
})
export class CobrangPagesModule { }
