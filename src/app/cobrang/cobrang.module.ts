import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CobrangPagesModule } from './pages/cobrang-pages.module';
import { CobrangRoutingModule } from './cobrang-routing.module';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module';



@NgModule({
  imports: [
    CommonModule,
    CobrangPagesModule,
    CobrangRoutingModule,
    RouterModule,
    MaterialModule
  ]
})
export class CobrangModule { }
