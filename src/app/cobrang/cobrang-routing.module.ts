import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CobrangLayoutComponent } from './pages/cobrang-layout/cobrang-layout.component';
import { NgloginComponent } from './pages/nglogin/nglogin.component';

const routes : Routes = [
  // {
  //   path: 'pages',
  //   component:
  // },
  {
    path: 'auth',
    component: CobrangLayoutComponent,
    children : [
      {
        path: 'login',
        component: NgloginComponent,
        // canActivate
      }
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CobrangRoutingModule { }
