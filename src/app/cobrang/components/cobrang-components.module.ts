import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgnavbarComponent } from './ngnavbar/ngnavbar.component';
import { CobrangRoutingModule } from '../cobrang-routing.module';
import { MaterialModule } from 'src/app/material/material.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    NgnavbarComponent
  ],
  imports: [
    CommonModule,
    CobrangRoutingModule,
    MaterialModule,
    RouterModule
  ],
  exports : [
    NgnavbarComponent
  ]
})
export class CobrangComponentsModule { }
