import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotyProvider } from 'src/app/providers/noty.provider';
import { TokenProvider } from 'src/app/providers/token.provider';
import { variables } from 'src/app/providers/variables';

@Component({
  selector: 'app-ngnavbar',
  templateUrl: './ngnavbar.component.html',
  styleUrls: ['./ngnavbar.component.scss']
})
export class NgnavbarComponent implements OnInit {

  // HTML image paths
  cobra_icon_img = 'https://www.pinclipart.com/picdir/big/121-1210042_viper-symbol-viper-snake-logo-png-clipart.png';
  cobra_letters_img = '../../../assets/img/cobra.png'
  home_img = 'https://icons-for-free.com/iconfiles/png/512/home-131964752879605498.png'
  logout_img = 'https://www.liceomodernosanmarcos.edu.co/wp-content/uploads/2020/09/salida.svg';
  folder_img = 'https://cdn-icons-png.flaticon.com/512/3767/3767084.png';
  // ----------------

  // Routes


  constructor(private tokenProvider : TokenProvider,
    private router : Router,
    private noty : NotyProvider) { }

  ngOnInit(): void {
  }

  close_sesion(){
    this.tokenProvider.destroy_data();
    this.noty.noty_info('Has cerrado sesión', 3000);
    this.router.navigate(['/almibar/login']);
  }

  get logged_in(){
    return this.tokenProvider.loggedIn;
  }


}
