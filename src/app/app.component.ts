import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TokenProvider } from './providers/token.provider';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend-training-main';

  constructor(private tokenProvider : TokenProvider){}

  get loggedIn(){
    return this.tokenProvider.loggedIn;
  }
}
